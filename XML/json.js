const bd =[
    {"Id":0,"libro":"Nombre: mitología griega", "autor":"Autor: Herbert Jennings", "idlibro": "Id del libro: 0001", 
    "correoeditorial":" Correo del editorial: sage@gmail.com", "categoria":"Categoria: Fantasia"},

    {"Id":1,"libro":"Nombre: Ana Karenina", "autor":"Autor: Leon tolstoi", "idlibro": "Id del libro: 0002", 
    "correoeditorial":" Correo del editorial: tecnos@gmail.com", "categoria":"Categoria: novela"},

    {"Id":2,"libro":"Nombre: último deseo", "autor":"Autor: Andrzej Sapkowski", "idlibro": "Id del libro: 0003", 
    "correoeditorial":" Correo del editorial: fragua@gmail.com", "categoria":"Categoria: Fantasia"},
    
    {"Id":9,"libro":"Nombre: Misery", "autor":"Autor: Stephen King", "idlibro": "Id del libro: 0004", 
    "correoeditorial":" Correo del editorial: arieleditorials@gmail.com", "categoria":"Categoria: terror"},
]

const libros = document.querySelectorAll('.nom_libros');

libros.forEach((libros)=>{
    libros.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('lib-id');
        bd.forEach((libros)=>{
            if(id == libros.Id){
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista"
                                        <div class="nom">
                                        <h2>Datos del libro:</h2>
                                        <h2 class="list-inline-item footer-menu"><a class="nav-link" style="color:#000000" href="index.html"> CERRAR VENTANA</a></h2>
                                        <p>${libros.libro}</p>
                                        <p>${libros.autor}</p>
                                        <p>${libros.idlibro}</p>
                                        <p>${libros.correoeditorial}</p>
                                        <p>${libros.categoria}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})

